/****************************************************************************
** CIS 15C
** Fall  2011
*************
**
** Homework 1-Stocks
**                          STACKS and
**              MULTIPLE SOURCE FILES PROGRAMS           25 Points
******************************************************************



****************************************
**
**  Written By: Eric Schultz
**
**  Date: 10/9/12
**
********************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

#ifdef _MSC_VER
#include <crtdbg.h>  // needed to check for memory leaks (Windows only!)
#endif

#include "stackADT.h"

int main (void)
{
//  Local Definitions

    FILE* fpIn;
    FILE* fpOut;
    char fileName[25];
    char fileOut[25];

    typedef struct stock{
                        char date[15];
                        char action;
                        int shares;
                        double price;
                        struct stock* link;
                        }STOCK;

    STACK* stack;
    STOCK* dataPtr;
    STOCK* dataOut;
    int sharesSold = 0;
    int sharesBought = 0;
    float tempCash = 0;
    float moneyS = 0;
    float moneyB = 0;
    float moneyFinal = 0;
    //int tempShares = 0;

//  Statements
	printf("\n\n\t\tHomework 1: STACK ADT\n\n");
	printf("\tThis program ...\n" );

    printf("\nEnter input file name here:\n");
    scanf("%s", fileName);

    if(!(fpIn = fopen(fileName, "r")))
        {
            printf("\nFile not found\n");
            exit(100);
        }

    //Creating stack to store stock information in.
    stack = createStack();
    //Allocating memory for the first node.
    dataPtr  = (STOCK*)malloc(sizeof(STOCK));

    while(fscanf(fpIn, "%s %c %d %lf", dataPtr->date, &dataPtr->action,
                                &dataPtr->shares, &dataPtr->price) != EOF)
    {
        //Pushing data onto the stack.
        pushStack(stack, dataPtr);
        //Allocating memory for every node necessary after the first.
        dataPtr  = (STOCK*)malloc(sizeof(STOCK));
    }


    printf("\nEnter name for file where stack is to be saved (with .txt): ");
    scanf("%s", fileOut);

    //Opening output save file.
    fpOut = fopen(fileOut, "w");

    //Processing and printing data to the screen, and the save file.
    while(!(emptyStack(stack)))
    {
        dataOut = popStack(stack);
        printf("\n%s %c %7d   %5.2f", dataOut->date, dataOut->action, dataOut->shares, dataOut->price);
        fprintf(fpOut, "%s %c %7d %5.2f \n", dataOut->date, dataOut->action, dataOut->shares, dataOut->price);
        if(toupper(dataOut->action) == 'S')
        {
            sharesSold += dataOut->shares;
            tempCash = (float)dataOut->shares * dataOut->price;
            moneyS += tempCash;
            printf("\t\t%5d * %4.2f \t= %10.2f", dataOut->shares, dataOut->price, tempCash);
        }
        else if(toupper(dataOut->action) == 'B')
        {
            sharesBought += dataOut->shares;
            tempCash = (float)dataOut->shares * dataOut->price;
            moneyB += tempCash;
            printf("\t\t%5d * %4.2f \t= %10.2f", dataOut->shares, dataOut->price, tempCash);
        }
        free(dataOut);
    }

    printf("\n\nTotal number of shares bought: %d", sharesBought);
    printf("\n\nTotal number of shares sold: %d\n", sharesSold);

    moneyFinal = moneyS - moneyB;

    printf("\n\nGain: $%10.2f - $%10.2f = $%10.2f", moneyS, moneyB, moneyFinal);

/*  while(tempShares <= sharesSold)  // Wasn't sure where to put this to make it work.
    {
        printf("\nGOT HERE 1.25");
        if(toupper(dataPtr->action) == 'B')
        {
            printf("\nGOT HERE 1.5");
            tempShares+=dataPtr->shares;
            dataPtr = popStack(stack);
        }
        else
        {
            dataPtr = popStack(stack);
        }
    }

    printf("%d shares left out of %d shares at $%.2f each.", something, dataOut->shares, dataOut->price); */


	printf("\n\n\t\tThank you for using the program,"
		   "\n\t\tHave a great day!\n");

    #ifdef _MSC_VER
	printf( _CrtDumpMemoryLeaks() ? "Memory Leak\n" : "No Memory Leak\n");
	#endif

	return 0;

} // main

