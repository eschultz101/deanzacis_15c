/*********************************************************************************
** CIS 26B  Spring, 2013
** Advanced C
**********************************************************************************
**
** Homework 1: Review
**        Arrays, Pointers, Strings, Structures, and
**        Dynamic Allocation of Memory
** 
** Program accepts a file of state/cities and their high temperatures, then 
** calculates the average high temperature for each state/city and writes to file.
** 
**********************************************************************************
**
**  Written By: Eric Schultz
**
**  Date: 4/17/2013
**********************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <string.h>
#include "wrapper.h"

#ifdef _MSC_VER
#include <crtdbg.h>
#endif

#define BSIZE 5

typedef struct city {
    char* name;
    int temperature;
} CITY;

CITY* getData(int* actualSize, char inputName[]);
void insertionSort(CITY* cityList, int actualSize);
void printList(CITY* cityList, int actualSize);
void calcAvgs(CITY* cityList, int actualSize, char outputName[]);
void destroyList(CITY* cityList, int actualSize);

int main( void ) {

    CITY* cityList;
    char inputName[25];
    char outputName[25];
    int actualSize;
    
    printf("Enter name of input file: ");
    scanf("%s", &inputName);
    
    cityList = getData(&actualSize, inputName);
    insertionSort(cityList, actualSize);
    printList(cityList, actualSize);
    printf("\n\nEnter name for output file (use .txt): ");
    scanf("%s", &outputName);
    calcAvgs(cityList, actualSize, outputName);
    destroyList(cityList, actualSize);

    
    #ifdef _MSC_VER
    printf( _CrtDumpMemoryLeaks() ? "Memory Leak\n" : "No Memory Leak\n");
    #endif
    
    return 0;
}

CITY* getData(int* actualSize, char inputName[]) {
    
    CITY* cityList = NULL;
    int tTemp;
    char tName[100];
    FILE* fpIn = fopen(inputName, "r");
    int arrSize = BSIZE;
    int i = 0;
    cityList = (CITY*)xMalloc(BSIZE*sizeof(CITY));
    
    while(fscanf(fpIn, "%[^:] %*c %d", &tName, &tTemp) != EOF) {
        if(i < arrSize) {
            cityList[i].temperature = tTemp;
            cityList[i].name = (char*)xMalloc((strlen(tName) + 1)*sizeof(char));
            strcpy(cityList[i].name, tName);
            i++;
        }
        else {
            //using xRealloc to add BSIZE city slots to cityList if too small
            arrSize += BSIZE;
            cityList = xRealloc(cityList, arrSize*(sizeof(CITY)));
            cityList[i].temperature = tTemp;
            cityList[i].name = (char*)xMalloc((strlen(tName) + 1)*sizeof(char));
            strcpy(cityList[i].name, tName);
            i++;
        }
    }
    
    *actualSize = i;
    
    //Shrinking array to fit
    cityList = xRealloc(cityList, i*(sizeof(CITY)));
    
    fclose(fpIn);

    return cityList;
}

void insertionSort(CITY* cityList, int actualSize) {
    
    int i, j;
    CITY temp;
    char tName[100];
    
    for(i = 0; i < actualSize; i++) {
        strcpy(tName, (cityList[i]).name);
        temp = cityList[i];
        for(j = i - 1; j >= 0; j--) {
            if(strcmp(cityList[j].name, tName) <= 0) break;
            cityList[j + 1] = cityList[j];
        }
        cityList[j + 1] = temp;
    }

    return;
}

void calcAvgs(CITY* cityList, int actualSize, char outputName[]) {
    
    FILE* fpOut = fopen(outputName, "w");
    int i, j, sum = 0, count = 0, spot = 0;
    double avg;
    fprintf(fpOut, "\nEach city's average high temperature:");
    
    for(i = 0; i < actualSize; i = spot) {
        for(j = 0; j < actualSize; j++) {
            if(strcmp(cityList[i].name, cityList[j].name) == 0) {
                sum += cityList[j].temperature;
                count++;
                spot++;
            }
        }
        avg = (double)sum/count;
        fprintf(fpOut, "%s %.2f", cityList[i].name, avg);
        sum = 0;
        count = 0;
    }
    
    fclose(fpOut);
    printf("Write to file %s successful.", outputName);
    
    return;
}

void printList(CITY* cityList, int actualSize) {
    int j;
    printf("Full list of states, cities and temperatures:");
    for(j = 0; j < actualSize; j++) {
        printf("%s %d", cityList[j].name, cityList[j].temperature);
    }
}

void destroyList(CITY* cityList, int actualSize) {
    int i;
    for(i = 0; i < actualSize; i++) {
        free(cityList[i].name);
    }
    free(cityList);
}
