/* 
 * File:   main.c
 * Author: Eric
 *
 * Created on April 17, 2013, 2:55 PM
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "wrapper.h"

#define BSIZE 5

typedef struct city {
    char* name;
    int temperature;
} CITY;

void getData(CITY[] cityList, char[] inputName, char[] outputName);

int main( void ) {

    CITY[BSIZE] cityList = (CITY*)xMalloc(BSIZE*sizeof(CITY));
    char[] inputName = "input file"; //change when done debugging!
    char[] outputName;
    
    //printf("Enter name of input file: ");
    //scanf("%s", &inputName);
    //printf("\nEnter name for output file: ");
    //scanf("%s", &outputName);
    
    getData(&cityList, inputName, outputName);
    
    return 0;
}

void getData(CITY[] cityList, char[] inputName, char[] outputName) {
    
    int tTemp;
    char[100] tName;
    FILE* fpIn = fopen(inputName, "r");
    int i = 0;
    
    while(fscanf(fpIn, "%[^:] %d", &tName, tTemp) != EOF) {
        if(cityList[i]) {
            (cityList[i])->temperature = tTemp;
            (cityList[i])->name = (char*)xMalloc(strlen(tName)*sizeof(char));
            strcpy((cityList[i])->name, tName);
            i++;
        }
        else {
            //use xRealloc to add BSIZE cities to cityList if too small
            cityList = (CITY*)xRealloc(cityList, BSIZE*(sizeof(CITY)));
            if(cityList[i]) {
                (cityList[i])->temperature = tTemp;
                (cityList[i])->name = (char*)xMalloc(strlen(tName)*sizeof(char));
                strcpy((cityList[i])->name, tName);
                i++;
            }
        }
    }
    
    return;
}
