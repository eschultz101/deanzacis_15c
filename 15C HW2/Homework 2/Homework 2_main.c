/*
 * File:   main.cpp
 * Author: Eric
 *
 * Created on December 8, 2012, 2:05 PM
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "HW2_Header.h"

void printMenu(HEADER* doubleList);

//When adding destinations & costs: search for first aCode, insert second aCode and cost; then search for second aCode, insert first aCode and cost.
//Search in doubly linked list and singly linked list? by airport code A->Z

int main(void) {
    HEADER* doubleList;
    DLLNODE* walker;
    char aCodeIn[4];
    char locIn[30];
    int count = 0;

    //Creating doubly linked list of airports
    doubleList = createDLList();

    if (!doubleList) {
        printf("\aCannot create double list\n"),
                exit(100);
    }
    //Set counter to zero
    doubleList->count = 0;

    //Scanning in data for airports
    printf("GOT HERE\n");
    dataIn(doubleList, aCodeIn, locIn);
    printf("GOT HERE\n");

    //Creating singly linked lists for each airport's destinations
    /*walker = doubleList->head->forw;

    while(count <= doubleList->count)
    {
        walker->headSLL = addNodeSLL(doubleList);
        count++;
        walker = walker->forw;
    }*/

    //Print menu
    printMenu(doubleList);

    return 0;
}

void dataIn(HEADER* doubleList, char aCodeIn[], char locIn[]) {
    FILE* fpIn;
    int success;
    fpIn = fopen("airports.txt", "r");
    if (!fpIn) {
        printf("\aError opening input file\n"),
                exit(110);
    }

    while (fscanf(fpIn, "%s %[^\n]", aCodeIn, locIn) != EOF) {
        success = addNodeDLL(doubleList, aCodeIn, locIn);

        if (success == 1) {
            printf("\n%d Node(s) successfully added\n", doubleList->count);
        }
    }

    return;
}

void printMenu(HEADER* doubleList) {
    char choice;

    printf("MENU\n");
    printf("Enter F to print the list of airports forward\n"
            "Enter B to print the list of airports backward\n"
            "Enter D to list the airports and their destinations\n"
            "Enter S to search for a specific airport by its airport code\n"
            "Enter C to list each airport's cheapest destination\n\n"
            "Enter choice: ");
    scanf("%c", &choice);

    switch (choice) {
        case 'F':
            printForward(doubleList);
            break;
        case 'B':
            printBackward(doubleList);
            break;
        case 'D':
            listDestinations(doubleList);
            break;
        case 'S':
            searchManager(doubleList);
            break;
        case 'C':
            listCheapest(doubleList);
            break;
    }

    return;
}


//Need createDLList function using header struct.
//Need printforward, backward functions with number of nodes included.
//Need to be able to list each destination for each airport, along with number of destinations.
//Need search manager that can find an airport (by its aCode) and list its destinations.  Search repeats until user enters "end".
//Need to list for each airport its cheapest destinations.
//Need function to add nodes to list.

HEADER* createDLList() {
    //Local definitions
    HEADER* doubleList;

    //Statements
    doubleList = (HEADER*) malloc(sizeof (HEADER));
    doubleList->head = (DLLNODE*) malloc(sizeof (DLLNODE));
    doubleList->tail = (DLLNODE*) malloc(sizeof (DLLNODE));

    if (doubleList) {
        doubleList->head->back = NULL;
        doubleList->count = 0;
        doubleList->tail->forw = NULL;
        doubleList->head->forw = doubleList->tail;
        doubleList->tail->back = doubleList->head;

    } // if

    return doubleList;
} // createDLList

int addNodeDLL(HEADER* doubleList, char aCodeIn[], char locIn[]) {

    DLLNODE* walker;
    DLLNODE* pNew; //New node to be added
    int count = 1;

    //	Statements

    if (!(pNew = (DLLNODE*) malloc(sizeof (DLLNODE))))
        return 0;
    if (!(pNew->aNode = (AIRPORT*) malloc(sizeof (AIRPORT))))
        return 0;

    strcpy(pNew->aNode->aCode, aCodeIn);
    pNew->aNode->loc = (char*) malloc(strlen(locIn));
    strcpy(pNew->aNode->loc, locIn);

    if (doubleList->count == 0) //if list is empty
    {
        doubleList->head->forw = pNew;
        pNew->back = doubleList->head;
        pNew->forw = doubleList->tail;
        doubleList->tail->back = pNew;

        (doubleList->count)++;
        return 1; // success
    } else if (doubleList->count > 0) //if list is not empty
    {
        walker = doubleList->head->forw; // point walker to 1st element

        // while(walker->forw != doubleList->tail && count <= doubleList->count)
        while(count <= doubleList->count) {
            if (strcmp(aCodeIn, walker->aNode->aCode) > 0) {
                walker = walker->forw; //Finds alphabetically correct slot for airport
                count++;
            } else {
                //walker = walker->back;
                count = (doubleList->count) + 1;
            }
        }
        walker = walker->back;

        walker->forw->back = pNew;
        pNew->forw = walker->forw;
        walker->forw = pNew;
        pNew->back = walker;

        (doubleList->count)++;

        return 1; //success
    }

    return 0; // failed to add node
}

void printForward(HEADER* doubleList) {
    //local variables
    int count = 0;

    DLLNODE* walker = doubleList->head->forw;

    printf("\nAirport code\t\tLocation\n");

    while (count <= doubleList->count) {
        printf("%s\t\t%s\n", walker->aNode->aCode, walker->aNode->loc);
        walker = walker->forw;
        count++;
    }

    return;
}

void printBackward(HEADER* doubleList) {
    int count = 0;

    printf("\nAirport code\t\tLocation\n");

    DLLNODE* walker = doubleList->tail;

    while (count <= doubleList->count) {
        printf("%s\t\t%s\n", walker->aNode->aCode, walker->aNode->loc);
        walker = walker->back;
        count++;
    }

    return;
}

void listDestinations(HEADER* doubleList) //Lists all airports and their destinations, along with number of destinations
{

    return;
}

void searchManager(HEADER* doubleList) {

    return;
}

void listCheapest(HEADER* doubleList) //Prints list of cheapest destination for each airport
{

    return;
}

//Need function to create a singly linked list for every doubly linked list node there is.  Then set DLLNODE* headSLL pointing to first node in each list.
//Need function to add destinations to SLLs in each DLL node (both aCode and aCode2 must be processed for each price)

/*SLLNODE* createSLLists()
{
    SLLNODE* head;

    head = (SLLNODE*)malloc(sizeof(SLLNODE));

    return head;
}*/

SLLNODE* addNodeSLL(HEADER* doubleList) {
    FILE* fpDest;

    char aCodeIn[4];
    char aCodeIn2[4];
    int price;
    int count = 0;
    DLLNODE* walker;
    SLLNODE* pNew;

    walker = doubleList->head->forw;

    fpDest = fopen("prices.txt", "r");

    while (fscanf(fpDest, "%s %s %d", aCodeIn, aCodeIn2, &price) != EOF) {
        while (count <= doubleList->count) {
            if (walker->aNode->aCode == aCodeIn) {
                pNew = (SLLNODE*) malloc(sizeof (SLLNODE));
                pNew->dest->destACode = (char*) malloc(strlen(aCodeIn2) + 1);
                strcpy(pNew->dest->destACode, aCodeIn2);
                pNew->dest->cost = price;
                (pNew->countSLL)++;
            } else if (walker->aNode->aCode == aCodeIn2) {
                pNew = (SLLNODE*) malloc(sizeof (SLLNODE));
                pNew->dest->destACode = (char*) malloc(strlen(aCodeIn) + 1);
                strcpy(pNew->dest->destACode, aCodeIn);
                pNew->dest->cost = price;
                (pNew->countSLL)++;
            }
            walker = walker->forw;
            count++;
        }
    }

    return pNew;
}
