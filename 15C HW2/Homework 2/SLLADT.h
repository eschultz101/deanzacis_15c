typedef struct destination{
                            char destACode[4]; //Airport code
                            int cost; //Cost
                            }DESTINATION;


typedef struct sllnode{
                        struct sllnode* link; //pointer to next node in list
                        DESTINATION* dest; //destination node (location, cost)
                        int countSLL;
                        }SLLNODE; //Singly linked list node


SLLNODE* createSLLists ()
