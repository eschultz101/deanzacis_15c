
typedef struct destination{
                            char* destACode; //Airport code
                            int cost; //Cost
                            }DESTINATION;

typedef struct sllnode{
                        struct sllnode* link; //pointer to next node in list
                        DESTINATION* dest; //destination node (location, cost)
                        int countSLL;
                        }SLLNODE; //Singly linked list node

typedef struct airport{
                        char aCode[4]; //Airport code
                        char* loc; //Dynamic allocation for location name
                        }AIRPORT;

typedef struct dllnode{
                        struct dllnode* forw;//forward pointer
                        struct dllnode* back;//back pointer
                        AIRPORT* aNode;//Airport node (code, location)
                        int countDLL;//count (useful in search)
                        SLLNODE* headSLL;//pointer to destination singly linked list
                        }DLLNODE; //Doubly linked list node

typedef struct header{
                        DLLNODE* head;//pointer to head dummy node (in DLL)
                        int count; //keeps track of total number of nodes in DLL (other than dummies)
                        DLLNODE* tail;//pointer to tail dummy node (in DLL)
                        }HEADER;


//SLLNODE* createSLLists ()

HEADER* createDLList();
void printForward(HEADER* doubleList);
void printBackward(HEADER* doubleList);
void listDestinations(HEADER* doubleList); //Lists all airports and their destinations, along with number of destinations
void searchManager(HEADER* doubleList); //Searches DLL by airport code
void listCheapest(HEADER* doubleList); //Prints list of cheapest destination for each airport
int addNodeDLL (HEADER* doubleList, char aCodeIn[], char locIn[]);
SLLNODE* addNodeSLL(HEADER* doubleList);
void dataIn(HEADER* doubleList, char aCodeIn[], char locIn[]);
