/*#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "DLLADT.h"

//Need createDLList function using header struct.
//Need printforward, backward functions with number of nodes included.
//Need to be able to list each destination for each airport, along with number of destinations.
//Need search manager that can find an airport (by its aCode) and list its destinations.  Search repeats until user enters "end".
//Need to list for each airport its cheapest destinations.
//Need function to add nodes to list.

HEADER* createDLList()
{
    //Local definitions
    HEADER* doubleList;

    //Statements
    doubleList = (HEADER*)malloc(sizeof(HEADER));
    doubleList->head = (DLLNODE*)malloc(sizeof(DLLNODE));
    doubleList->tail = (DLLNODE*)malloc(sizeof(DLLNODE));

    if(doubleList)
    {
        doubleList->head->back = NULL;
        doubleList->count = 0;
        doubleList->tail->forw = NULL;
        doubleList->head->forw = doubleList->tail;
        doubleList->tail->back = doubleList->head;
    } // if

    return doubleList;
} // createDLList

int addNodeDLL (HEADER* doubleList, char aCodeIn[], char locIn[])
{

    DLLNODE* walker;
    DLLNODE* pNew; //New node to be added

    walker = doubleList->head;

//	Statements

    if (!(pNew = (DLLNODE*)malloc(sizeof(DLLNODE))))
        return 0;
    if (!(pNew->aNode = (AIRPORT*)malloc(sizeof(AIRPORT))))
        return 0;

    strcpy(pNew->aNode->aCode, aCodeIn);
    pNew->aNode->loc = (char*)malloc(strlen(locIn));
    strcpy(pNew->aNode->loc, locIn);

    if(doubleList->count == 0) //if list is empty
    {
        doubleList->head->forw = pNew;
        pNew->back = doubleList->head;
        pNew->forw = doubleList->tail;
        doubleList->tail->back = pNew;

        (doubleList->count)++;
        return 1; // success
    }
    else if(doubleList->count > 0) //if list is not empty
    {
        printf("GOT HERE addNodeDLL\n");
        walker = doubleList->head->forw;
        printf("GOT HERE addNodeDLL\n");
        while(strcmp(aCodeIn, walker->aNode->aCode) > 0 && walker != doubleList->tail)
        {
            printf("GOT HERE addNodeDLL\n");
            walker = walker->forw; //Finds alphabetically correct slot for airport
        }

        pNew->forw = walker->back->forw;
        pNew->back = walker->back;

        if(walker->forw == doubleList->tail) //if inserting at end of list
        {
            pNew->forw = doubleList->tail;
            doubleList->tail->back = pNew;
        }
        else
        {
            walker->forw->back = pNew; //not end of list
        }

        walker->back->forw = pNew;

        (doubleList->count)++;

        return 1; //success
    }

    return 0; // failed to add node
}

void printForward(HEADER* doubleList)
{
    //local variables
    int count = 0;

    DLLNODE* walker = doubleList->head->forw;

    printf("\nAirport code\t\tLocation\n");

    while(count <= doubleList->count)
    {
        printf("%s\t\t%s\n", walker->aNode->aCode, walker->aNode->loc);
        walker = walker->forw;
        count++;
    }

    return;
}

void printBackward(HEADER* doubleList)
{
    int count = 0;

    printf("\nAirport code\t\tLocation\n");

    DLLNODE* walker = doubleList->tail;

    while(count <= doubleList->count)
    {
        printf("%s\t\t%s\n", walker->aNode->aCode, walker->aNode->loc);
        walker = walker->back;
        count++;
    }

    return;
}

void listDestinations(HEADER* doubleList) //Lists all airports and their destinations, along with number of destinations
{

    return;
}

void searchManager(HEADER* doubleList)
{

    return;
}

void listCheapest(HEADER* doubleList) //Prints list of cheapest destination for each airport
{

    return;
}
*/
