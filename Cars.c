#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>

#define MAXLOT 100
typedef struct car{
    int miles;
    char make[20];
    char model[20];
    int year;
    float price;
    char vin[20];
}CAR;

char getOption(void);
void add(CAR *arrival);
int getData(CAR lot[]);
void print(CAR lot[], int num);
int removeRecord (CAR lot[], int num);
void filePrint(CAR lot[], int num);

int main (void)
{
    CAR lot[MAXLOT];
    int num; char action;
    CAR arrival;

    //Input data from file
    num = getData(lot);

    do
    {
        action = getOption();
        switch (action)
        {
        case 'A':
            if(num<MAXLOT)
            {
                add(&arrival);
                lot[num++] = arrival;
            }
            break;

        case 'R':
            num = removeRecord(lot, num);
            break;
        case 'Q':
            break;
        case 'P':print(lot, num); break;
        case 'F':filePrint(lot, num); break;
        }//switch
    }while (action != 'Q');
    return 0;
}

char getOption(void)
{
    char ch;
    printf("******************************");
    printf("\n\t\tMenu");
    printf("\n");
    printf("Enter A to add a car\n");
    printf("Enter R to remove a car\n");
    printf("Enter F to output to a file\n");
    printf("Enter P to print to screen\n");
    printf("Enter Q to quit\n");
    printf("*******************************");
    printf("\n\nPlease enter your choice: ");
    scanf(" %c", &ch);

    return toupper(ch);
}

void add(CAR *arrival)
{
    printf("\n");
    printf("Enter car year: ");
    scanf("%d", &arrival->year);
    printf("\nEnter car model: ");
    scanf("%19s", &arrival->model);
    printf("\nEnter car make: ");
    scanf("%19s", &arrival->make);
    printf("\nEnter car miles: ");
    scanf("%d", &arrival->miles);
    printf("\nEnter car price: ");
    scanf("%f", &arrival->price);
    printf("\nEnter car VIN: ");
    scanf("%s", arrival->vin);
    return;
}

int getData(CAR lot[])
{
    int  i;
    FILE* fpIn;

    if(!(fpIn = fopen("cars_data.txt", "r")))
    {
        printf("No such File\n");
        return 1;
    }
    for(i=0; i < 100 && fscanf(fpIn, "%d %19s %19s %d %f %19[^\n]", &lot[i].year, &lot[i].model,
        &lot[i].make, &lot[i].miles, &lot[i].price, lot[i].vin)!= EOF; i++);

    fclose(fpIn);

    return i;
}

void print(CAR lot[], int num)
{
    int i;

    printf("\n\n");
    for(i = 0; i < num; i++)
    {
        printf("%d %s %s %d %6.2f %s\n", lot[i].year, lot[i].model,
            lot[i].make, lot[i].miles, lot[i].price, lot[i].vin);
    }

    printf("\n\n");
    return;
}

int removeRecord (CAR lot[], int num)
{
    int i; int k;

    char vinToDelete[20];

    printf("\nEnter car VIN: ");
    scanf("%s", vinToDelete);

    for (i = 0; i < num; i++)
    {
        if(strcmp(lot[i].vin, vinToDelete) == 0)
        {
            for (k = i; k < num - 1; k++)
                lot[k] = lot[k + 1];

            num--;
        }//if
    }

    return num;
}

void filePrint(CAR lot[], int num)
{
    int i;
    FILE* fpOut;

    fpOut = fopen("cars_output.txt", "w");

    for(i = 0; i < num; i++)
    {
        fprintf(fpOut, "%d %s %s %d %6.2f %s\n", lot[i].year, lot[i].model,
            lot[i].make, lot[i].miles, lot[i].price, lot[i].vin);
    }

    printf("\n\nInfo printed to file cars_output.txt successfully.\n\n\n");

    fclose(fpOut);

    return;
}
