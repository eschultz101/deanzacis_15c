
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include "BSTADT.h"
#include <stdlib.h>

//Prototype declarations
void printMenu(void (*process)(void* dataPtr), BST_TREE* treeIDs, BST_TREE* treeNames);
void printByID (BST_TREE* treeIDs, void(*process)(void* dataPtr));
void printByName(BST_TREE* treeNames, void(*process)(void* dataPtr));
void printNode(void* dataPtr);

int main (void)
{
//	Local Definitions
    FILE* fpIn;
    BST_TREE* treeIDs;
    BST_TREE* treeNames;
    STUDENT* newStu;
    int addRes;
    int IDin;
    float gpaIn;
    char nameIn[30];
    int length;
    int (*compare)(void* argu1, void* argu2);
    void (*process)(void* dataPtr);

    compare = compareStu;
    treeIDs = BST_Create(compare);//tree with IDs as key

    compare = compareNames;
    treeNames = BST_Create(compare); //tree with names as key

    fpIn = fopen("gpa.txt", "r");

    newStu = (STUDENT*)malloc(sizeof(STUDENT));

    while(fscanf(fpIn, "%d %[^;] %*c %f", &IDin, nameIn, &gpaIn) != EOF)
        {
            newStu = (STUDENT*)malloc(sizeof(STUDENT));
            length = strlen(nameIn);
            newStu->name = (char*)malloc(length+1);
            strcpy(newStu->name, nameIn);
            newStu->id = IDin;
            newStu->gpa = gpaIn;
            addRes = BST_Insert(treeIDs, newStu);
            addRes = BST_Insert(treeNames, newStu);
        }

    printMenu(process, treeIDs, treeNames);

	return 0;
}	// main

void printMenu(int (*compare)(void* argu1, void* argu2), void (*process)(void* dataPtr),
                                                BST_TREE* treeIDs, BST_TREE* treeNames)
{

    char ch;

    printf("**** MENU ************************************\n"
           "**** Enter A to print list sorted by PIN  ****\n"
           "**** Enter B to print list sorted by name ****\n"
           "**** Enter C to search by PIN             ****\n"
           "**** Enter D to search by name            ****\n" // all matching names should be displayed
           "**** Enter E to add a new student         ****\n"
           "**** Enter F to print trees by level      ****\n"
           "**** Enter M to print the menu            ****\n"
           "**** Enter Q to quit                      ****\n"
           "**********************************************\n"); // destroy trees at this point
    printf("\nEnter choice: ");


    while(scanf("%c", &ch) == 1)
    {
        switch(toupper(ch))
        {
            case 'A': printByID(treeIDs, process); break;
            case 'B': printByName(treeNames, process); break;
            case 'C': searchID(compare, treeIDs); break;
            case 'D': searchName(compare, treeNames); break;
            case 'E': break;
            case 'F': breadthFirst(treeIDs, treeNames); break;
            case 'M': printMenu(process, treeIDs, treeNames); break;
            case 'Q': printf("Quitting...\n"); BST_Destroy(treeIDs); BST_Destroy(treeNames); exit(0);
        }
    }
    return;
}

/*	====================== compareStu ======================
	Compare two student id's and return low, equal, high.
	    Pre  stu1 and stu2 are valid pointers to students
	    Post return low (-1), equal (0), or high (+1)
*/
int  compareStu   (void* stu1, void* stu2)
{
//	Local Definitions
	STUDENT s1 = *((STUDENT*)stu1);
    STUDENT s2 = *((STUDENT*)stu2);

//	Statements
	if ( s1.id < s2.id)
	      return -1;

	if ( s1.id == s2.id)
	      return 0;

	return +1;
    // return s1.id - s2.id
}	// compareStu

/*	====================== compareNames ======================
	Compare two student's names and return low, equal, high.
	    Pre  stu1 and stu2 are valid pointers to students
	    Post return low (-1), equal (0), or high (+1)
*/
int  compareNames   (void* stu1, void* stu2)
{
//	Local Definitions
	STUDENT s1 = *((STUDENT*)stu1);
    STUDENT s2 = *((STUDENT*)stu2);

//	Statements
	return strcmp(s1.name, s2.name);
}	// compareStu

void printByID (BST_TREE* treeIDs, void(*process)(void* dataPtr))
{
    process = printNode;
    BST_Traverse(treeIDs, process);
    return;
}

void printByName(BST_TREE* treeNames, void(*process)(void* dataPtr))
{
    process = printNode;
    BST_Traverse(treeNames, process);
    return;
}

void printNode(void* dataPtr)
{
    STUDENT stu = *((STUDENT*)dataPtr);
    printf("%d %s %1.1f\n", stu.id, stu.name, stu.gpa);

    return;
}

void searchName(int (*compare)(void* argu1, void* argu2), BST_TREE* treeNames)
{
    char targetName[20];

    printf("\nEnter name of student to search for them in the form 'Last, First'\n");
    scanf("%s", targetName);

    STUDENT* targetStu;
    targetStu = (STUDENT*)malloc(sizeof(STUDENT));

    targetStu->name = malloc(strlen(targetName)+1);
    strcpy(targetStu->name, targetName);

    compare = compareNames;
    BST_Retrieve(treeNames, targetStu);

    return;
}

void searchID(int (*compare)(void* argu1, void* argu2), BST_TREE* treeIDs)
{
    int targetID;

    printf("\nEnter PIN of student to search for them\n");
    scanf("%d", targetID);

    STUDENT* targetStu;
    targetStu = (STUDENT*)malloc(sizeof(STUDENT));

    targetStu->id = targetID;

    compare = compareStu;
    BST_Retrieve(treeIDs, targetStu);

    return;
}
/*
void search()
{

    return;
}
*/
void breadthFirst(BST_TREE* treeIDs, BST_TREE* treeNames)
{

    NODE* currNode;
    QUEUE* bfQueue;
    bfQueue = createQueue();
    char treeCh;
    int res;

    printf("\nEnter N for names tree and I for IDs tree: ");
    scanf(" %c", &treeCh);
    printf("\n");

    if(toupper(treeCh) == 'I')
    {
        currNode = treeIDs->root;
    }
    else if(toupper(treeCh) == 'N')
    {
        currNode = treeNames->root;
    }
    else
    {
        printf("\nInvalid input, going to menu\n");
    }

    while(currNode != NULL)
    {
        printNode(currNode->dataPtr);

        if(currNode->left != NULL)
        {
            enqueue(bfQueue, currNode->left);
        }
        if(currNode->right != NULL)
        {
            enqueue(bfQueue, currNode->right);
        }
        if(!(emptyQueue(bfQueue)))
        {
            dequeue(bfQueue, &currNode);
        }
        else
        {
            currNode = NULL;
        }
    }
    destroyQueue(bfQueue);
    return;
}
