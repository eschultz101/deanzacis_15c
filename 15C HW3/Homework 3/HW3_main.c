/*
Homework 3

	   Written by: Eric Schultz
	   Date: 11/5/12
*/

#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include "BSTADT.h"
#include <stdlib.h>

int main (void)
{
//	Local Definitions
    FILE* fpIn;
    BST_TREE* treeIDs;
    BST_TREE* treeNames;
    STUDENT* newStu;
    int addRes;
    int IDin;
    float gpaIn;
    char nameIn[30];
    int length;
    int (*compare)(void* argu1, void* argu2);
    void (*process)(void* dataPtr);

    compare = compareStu;
    treeIDs = BST_Create(compare);//tree with IDs as key

    compare = compareNames;
    treeNames = BST_Create(compare); //tree with names as key

    fpIn = fopen("gpa.txt", "r");

    newStu = (STUDENT*)malloc(sizeof(STUDENT));

    while(fscanf(fpIn, "%d %[^;] %*c %f", &IDin, nameIn, &gpaIn) != EOF)
        {
            newStu = (STUDENT*)malloc(sizeof(STUDENT));
            length = strlen(nameIn);
            newStu->name = (char*)malloc(length+1);
            strcpy(newStu->name, nameIn);
            newStu->id = IDin;
            newStu->gpa = gpaIn;
            addRes = BST_Insert(treeIDs, newStu);
            addRes = BST_Insert(treeNames, newStu);
        }

    //Print menu which handles most other functions
    printMenu(process, treeIDs, treeNames);

	return 0;
}	// main

void printMenu(void (*process)(void* dataPtr), BST_TREE* treeIDs, BST_TREE* treeNames)
{

    char ch;

    printf("**** MENU ************************************\n"
           "**** Enter A to print list sorted by PIN  ****\n"
           "**** Enter B to print list sorted by name ****\n"
           "**** Enter C to search by PIN             ****\n"
           "**** Enter D to search by name            ****\n" // all matching names should be displayed
           "**** Enter E to add a new student         ****\n"
           "**** Enter F to print trees by level      ****\n"
           "**** Enter M to print the menu            ****\n"
           "**** Enter Q to quit                      ****\n"
           "**********************************************\n"); // destroy trees at this point
    printf("\nEnter choice: ");


    while(scanf("%c", &ch) == 1)
    {
        switch(toupper(ch))
        {
            case 'A': printByID(treeIDs, process); break;
            case 'B': printByName(treeNames, process); break;
            case 'C': searchID(treeIDs); break;
            case 'D': searchName(treeNames); break;
            case 'E': addStu(treeIDs, treeNames); break;
            case 'F': breadthFirst(treeIDs, treeNames); break;
            case 'M': printMenu(process, treeIDs, treeNames); break;
            case 'Q': printf("Quitting...\n"); quit(treeIDs, treeNames, process); exit(0);
        }
    }
    return;
}

/*	====================== compareStu ======================
	Compare two student id's and return low, equal, high.
	    Pre  stu1 and stu2 are valid pointers to students
	    Post return low (-1), equal (0), or high (+1)
*/
int  compareStu   (void* stu1, void* stu2)
{
//	Local Definitions
	STUDENT s1 = *((STUDENT*)stu1);
    STUDENT s2 = *((STUDENT*)stu2);

//	Statements
	if ( s1.id < s2.id)
	      return -1;

	if ( s1.id == s2.id)
	      return 0;

	return +1;
    // return s1.id - s2.id
}	// compareStu

/*	====================== compareNames ======================
	Compare two student's names and return low, equal, high.
	    Pre  stu1 and stu2 are valid pointers to students
	    Post return low (-1), equal (0), or high (+1)
*/
int  compareNames   (void* stu1, void* stu2)
{
//	Local Definitions
	STUDENT s1 = *((STUDENT*)stu1);
    STUDENT s2 = *((STUDENT*)stu2);

//	Statements
	return strcmp(s1.name, s2.name);
}	// compareNames

void printByID (BST_TREE* treeIDs, void(*process)(void* dataPtr))
{
    process = printNode;
    BST_Traverse(treeIDs, process);
    return;
}

void printByName(BST_TREE* treeNames, void(*process)(void* dataPtr))
{
    process = printNode;
    BST_Traverse(treeNames, process);
    return;
}

void printNode(void* dataPtr)
{
    STUDENT stu = *((STUDENT*)dataPtr);
    printf("%d %s %1.1f\n", stu.id, stu.name, stu.gpa);

    return;
}

void searchName(BST_TREE* treeNames)
{
    char targetName[30];

    printf("\nEnter name of student to search for (Last, First;): ");
    scanf("%*c %[^;] %*c", targetName);

    char* temp;
    temp = (char*)malloc(strlen(targetName)+1);
    strcpy(temp, targetName);

    compNames(treeNames->root, temp);

    printf("\nIf no students are shown, name was not found.\n");

    return;
}

void compNames(NODE* root, char* temp)
{
    if (root)
    {
        if(strcmp(temp, ((STUDENT*)root->dataPtr)->name) == 0)
        {
            printNode(root->dataPtr);
        }
        compNames(root->left, temp);
        compNames(root->right, temp);
    }

    return;
}

void searchID(BST_TREE* treeIDs)
{
    int targetID;
    NODE* dataOut;

    printf("Enter PIN of student to search for them: \n");
    scanf("%d", &targetID);

    STUDENT* tempID;
    tempID = (STUDENT*)malloc(sizeof(STUDENT));
    tempID->id = targetID;

    dataOut = BST_Retrieve(treeIDs, tempID);

    if(dataOut)
    {
        printNode(dataOut);
    }
    else
    {
        printf("Entered PIN does not exist, please enter valid PIN\n");
    }

    free(tempID);

    return;
}

int addStu(BST_TREE* treeIDs, BST_TREE* treeNames)
{
    char nameIn[30];
    STUDENT* stu;
    stu = (STUDENT*)malloc(sizeof(STUDENT));

    printf("\nEnter PIN number for new student: ");
    scanf("%d", &stu->id);
    printf("\nEnter new student's name (Last, First;): ");
    scanf("%*c %[^;] %*c", nameIn);
    printf("\nEnter new student's gpa: ");
    scanf("%f", &stu->gpa);

    stu->name = (char*)malloc(strlen(nameIn));
    strcpy(stu->name, nameIn);

    if(BST_Insert(treeIDs, stu))
    {
        printf("\nNew student inserted into ID tree successfully\n");
    }
    if(BST_Insert(treeNames, stu))
    {
        printf("\nNew student inserted into Names tree successfully\n");
    }

    return 0;
}

void breadthFirst(BST_TREE* treeIDs, BST_TREE* treeNames)
{

    NODE* currNode;
    QUEUE* bfQueue;
    bfQueue = createQueue();
    char treeCh;

    printf("\nEnter N for names tree and I for IDs tree: ");
    scanf(" %c", &treeCh);
    printf("\n");

    if(toupper(treeCh) == 'I')
    {
        currNode = treeIDs->root;
    }
    else if(toupper(treeCh) == 'N')
    {
        currNode = treeNames->root;
    }
    else
    {
        printf("\nInvalid input, going to menu\n");
    }

    while(currNode != NULL)
    {
        printNode(currNode->dataPtr);

        if(currNode->left != NULL)
        {
            enqueue(bfQueue, currNode->left);
        }
        if(currNode->right != NULL)
        {
            enqueue(bfQueue, currNode->right);
        }
        if(!(emptyQueue(bfQueue)))
        {
            dequeue(bfQueue, &currNode);
        }
        else
        {
            currNode = NULL;
        }
    }
    destroyQueue(bfQueue);
    return;
}

void freeNames(void* dataPtr)
{
    STUDENT stu = *((STUDENT*)dataPtr);

    free(stu.name);

    return;
}

void quit(BST_TREE* treeIDs, BST_TREE* treeNames, void(*process)(void* dataPtr))
{
    process = freeNames;

    BST_Traverse(treeIDs, process);
    BST_Traverse(treeNames, process);

    BST_Destroy(treeIDs);
    BST_Destroy(treeNames);

    return;
}
